//b1 khai báo mongoose
const mongoose = require('mongoose');

//khai báo schema trong mongose
const Schema = mongoose.Schema;

//khai báo các collection bằng SchemaOrderr
const orderSchema = new Schema({
    // _id: mongoose.Types.ObjectId,
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize:{
        type: String,
        require: true,
    },
    pizzaType:{
        type: String,
        require: true,
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "vouchers"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "drinks"
    },
    status: {
        type: String,
        require: true
    }

})

module.exports = mongoose.model("order",orderSchema);