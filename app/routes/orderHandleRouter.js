const express = require("express");
const { orderHandle, getAllDrinksbyDev } = require("../controllers/orderHandlecontroller");

//tạo rounter
const orderHandleRouter = express.Router();

orderHandleRouter.post('/devcamp-pizza365/orders',orderHandle);

orderHandleRouter.get('/devcamp-pizza365/drinks',getAllDrinksbyDev);
module.exports = {
    orderHandleRouter
}