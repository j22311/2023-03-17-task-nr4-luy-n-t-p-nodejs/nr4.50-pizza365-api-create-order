//import thư viện mongoose
const { response, request } = require('express');
const mongoose = require('mongoose');

// import module drink model 
 const drinkModel = require ('../models/drinkModel');   

 //-----------------------lấy hết dữ liệu nước uống
 const getAllDrinks = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    drinkModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show drinks',
         drink: data
      })
   })
      .catch((error)=> {
         response.status(500).json({
            message: `internal sever ${error}`
         })
      })
   
 }
 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getADrinkById = (request,response) => {
   //b1: thu thập dữ liệu
   const drinkId = request.params.drinkId;

   //b2: Validate dữ liệu
   if(!mongoose.Types.ObjectId.isValid(drinkId)){
      return response.status(400).json({
         status: 'Bad Request',
         message: `${drinkId} ko hợp lệ`
      })
   }
   //bb3: gọi model tìm dữ liêu
   drinkModel.findById(drinkId)
      .then((data) => {
         response.status(201).json({
            message: 'successful to find one ',
            drinks: data
         })
      })
      .catch((error) => {
         response.status(500).json({
            message: `internal sever ${error}`
         })
      })
}

 //---------------------------insert a drink
 const insertADrink = (request, response) => {
   
   // b1: thu thập dữ liệu
   const body = request.body;
    // b2: validate dữ liệu 
   if(!body.maNuocUong){
      return response.status(400).json({
         status: 'Bad request',
         message: 'maNuocUong ko hop le'
      })
   }
   if(!body.tenNuocUong){
      return response.status(400).json({
         status: 'Bad request',
         message: 'tenNuocUong ko hop le'
      })
   }

   if(isNaN(body.donGia) || body.donGia < 0){
      return response.status(400).json({
         status: 'bad request',
         message: 'donGia ko hop le'
      })
   }
    // b3: gọi model tạo dữ liệu 
    const newDrink = new drinkModel( {
      //  _id: mongoose.Types.ObjectId,
      maNuocUong : body.maNuocUong,
      tenNuocUong : body.tenNuocUong,
      donGia: body.donGia
    })
    console.log(newDrink);
   
   drinkModel.create(newDrink)
    .then((data) => {
         response.status(201).json({
            message: 'Successful to create new drinks',
            drink: data
         })
    })
    .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
 }
//-----------------update a drinks----------------------------
const updateADrinkById = (request,response) => {
   //b1: thu thập dữ liệu
   const drinkId = request.params.drinkId;
   const body = request.body;
   //b2: validate dữ liệu
   if(!mongoose.Types.ObjectId.isValid(drinkId)){
      return response.status(400).json({
         status: 'Bad request',
         message: `${drinkId} ko hợp lệ`
      })
   }
   if(!body.maNuocUong){
      return response.status(400).json({
         status: 'Bad request',
         message: 'maNuocUong ko hop le'
      })
   }
   if(!body.tenNuocUong){
      return response.status(400).json({
         status: 'Bad request',
         message: 'tenNuocUong ko hop le'
      })
   }

   if(isNaN(body.donGia) || body.donGia < 0){
      return response.status(400).json({
         status: 'bad request',
         message: 'donGia ko hop le'
      })
   }

    // b3: gọi model để update dữ liệu
    let newDrinkUpdate = {
      maNuocUong : body.maNuocUong,
      tenNuocUong : body.tenNuocUong,
      donGia: body.donGia
    }
   drinkModel.findByIdAndUpdate(drinkId,newDrinkUpdate)
      .then((data) => {
         response.status(201).json({
            message: 'Successful to create new drinks',
            drink: data
         })
      })
      .catch((error) => {
         response.status(500).json({
         message: `internal sever ${error}`
         })
      })

}
//--------------delete a drinkbyID----------------------
const deleteADrinkById = ((request,response) => {
   //b1: thu thập dữ liệu
   const drinkId = request.params.drinkId;
   //b2: kiểm tra dữ liệu
   if(!mongoose.Types.ObjectId.isValid(drinkId)){
      return response.status(400).json({
         status: 'Bad request',
         message: `${drinkId} ko hợp lệ`
      })
   }
   //b3: gọi model xóa dữ liệu
   drinkModel.findByIdAndDelete(drinkId)
   .then((data) => {
      response.status(201).json({
         message: 'Successful to delete a drinks ',
         drink: data
      })
   })
   .catch((error) => {
      response.status(500).json({
      message: `internal sever ${error}`
      })
   })
})

 module.exports = {
   getAllDrinks,
   insertADrink,
   getADrinkById,
   updateADrinkById,
   deleteADrinkById
 }