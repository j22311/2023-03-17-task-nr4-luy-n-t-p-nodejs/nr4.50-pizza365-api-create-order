//khai báo thư viện mongoose
const { request, response } = require('express');
const mongoose = require('mongoose');

//import lớp user Model
const userModel = require('../models/userModel');


//-----------------------lấy hết dữ liệu user

const getAllUser = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    
    userModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all users',
         drink: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
 }
 const getAllUserLimit = (request,response) => {
   // b1: thu thập dữ liệu
   // b2: validate dữ liệu 
   // b3: gọi model tạo dữ liệu 
   const limit = request.params.limit;
   console.log(limit);
   userModel.find().limit(limit)
  .then((data) => {
     response.status(201).json({
        message: 'Successful to show all users',
        drink: data
     })
  })
  .catch((error)=> {
     response.status(500).json({
        message: `internal sever ${error}`
     })
   })
}
const getAllUserSkip = (request,response) => {
   // b1: thu thập dữ liệu
   // b2: validate dữ liệu 
   // b3: gọi model tạo dữ liệu 
   const skip = request.params.skip;
   console.log(skip);
   userModel.find().skip(skip)
  .then((data) => {
     response.status(201).json({
        message: 'Successful to show all users',
        user: data
     })
  })
  .catch((error)=> {
     response.status(500).json({
        message: `internal sever ${error}`
     })
   })
}
const getAllUserSort = (request,response) => {
   // b1: thu thập dữ liệu
   // b2: validate dữ liệu 
   // b3: gọi model tạo dữ liệu 
   
   
   userModel.find().sort({ fullName: 'asc' })
  .then((data) => {
     response.status(201).json({
        message: 'Successful to show all users',
        user: data
     })
  })
  .catch((error)=> {
     response.status(500).json({
        message: `internal sever ${error}`
     })
   })
}

const getAllUserSkipLimit = (request,response) => {
   // b1: thu thập dữ liệu
   // b2: validate dữ liệu 
   // b3: gọi model tạo dữ liệu 
   const skip = request.params.skip;
   limit = request.params.skip;
  
   userModel.find().skip(skip).limit(limit)
  .then((data) => {
     response.status(201).json({
        message: 'Successful to show all users',
        user: data
     })
  })
  .catch((error)=> {
     response.status(500).json({
        message: `internal sever ${error}`
     })
   })
}
const getAllUserSkipLimitSort = (request,response) => {
   // b1: thu thập dữ liệu
   // b2: validate dữ liệu 
   // b3: gọi model tạo dữ liệu 
   const skip = request.params.skip;
   limit = request.params.skip;
  
   userModel.find().skip(skip).limit(limit).sort({ fullName: 'asc' })
  .then((data) => {
     response.status(201).json({
        message: 'Successful to show all users',
        user: data
     })
  })
  .catch((error)=> {
     response.status(500).json({
        message: `internal sever ${error}`
     })
   })
}
 //lấy một dữ liệu voucher bằng id
//-------------------------get a voucher by ID 
const getAUserById = (request,response) => {
    //b1: thu thập dữ liệu
    const userId = request.params.userId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${userId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    userModel.findById(userId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one userId ',
             voucher: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }

 //------------------insert one User ---------------------------
 const insertAUser = (request, response) => {

    //b1: thu thập dữ liệu
    const body = request.body;
    const voucherId = request.params.voucherId;
    console.log(body);
    //b2: validate dữ liệu
    if(!body.fullName){
        return response.status(400).json({
           status: 'Bad request',
           message: 'fullname ko hop le'
        })
    }
    if(!body.email){
        return response.status(400).json({
           status: 'Bad request',
           message: 'email ko hop le'
        })
    }
    if(!body.address){
        return response.status(400).json({
           status: 'Bad request',
           message: 'address ko hop le'
        })
    }
    if(isNaN(body.phone) || body.phone < 0){
        return response.status(400).json({
           status: 'Bad request',
           message: 'phone ko hop le'
        })
    }

    // b3: gọi model tạo dữ liệu 
    const newUser = new userModel( {
        //  _id: mongoose.Types.ObjectId,
        fullName : body.fullName,
        email : body.email,
        address: body.address,
        phone: body.phone
    })
    console.log(newUser);
   
    userModel.create(newUser)
    .then((data) => {
         response.status(201).json({
            message: 'Successful to create new user',
            user: data
         })

    })
    .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })

 }
 //------------------update A User by ID ---------------------------
 const updateAUser = (request, response) => {
    //b1: thu thập dữ liệu
    const body = request.body;
    const userId = request.params.userId;
    console.log(body);
    //b2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
           status: 'Bad Request',
           message: `${userId} ko hợp lệ`
        })
     }
    if(!body.fullName){
        return response.status(400).json({
           status: 'Bad request',
           message: 'fullName ko hop le'
        })
    }
    if(!body.email){
        return response.status(400).json({
           status: 'Bad request',
           message: 'email ko hop le'
        })
    }
    if(!body.address){
        return response.status(400).json({
           status: 'Bad request',
           message: 'address ko hop le'
        })
    }
    if(isNaN(body.phone) || body.phone < 0){
        return response.status(400).json({
           status: 'Bad request',
           message: 'phone ko hop le'
        })
    }

    // b3: gọi model tạo dữ liệu 
    const newUser =  {
        //  _id: mongoose.Types.ObjectId,
        fullName : body.fullName,
        email : body.email,
        address: body.address,
        phone: body.phone
    }
    console.log(newUser);
   
    userModel.findByIdAndUpdate(userId,newUser)
    .then((data) => {
         response.status(201).json({
            message: 'Successful to update new user',
            user: data
         })
        
    })
    .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })

 }
 //////////-----------------delete a user by Id-----------------//////
 const deleteUserbyId = (request, response) => {
    //b1: thu thập dũ liệu
    const userId = request.params.userId;
   // b2:validate userID
   if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
        status: 'Bad Request',
        message: `${userId} ko hợp lệ`
        })
    }
    userModel.findByIdAndDelete(userId)
    .then((data) => {
        response.status(201).json({
           message: 'Successful to delete new user',
        })
   })
   .catch((error)=> {
     response.status(500).json({
        message: `internal sever ${error}`
     })
   })

 }
 module.exports = {
    getAllUser,
    insertAUser,
    getAUserById,
    updateAUser,
    deleteUserbyId,

    getAllUserLimit,
    getAllUserSkip,
    getAllUserSort,
    getAllUserSkipLimit,
    getAllUserSkipLimitSort
 }