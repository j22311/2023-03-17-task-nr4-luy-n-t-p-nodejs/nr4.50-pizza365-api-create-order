//import thư viện mongoose
const { request, response } = require('express');
const mongoose = require('mongoose');

//import lớp order Model
const orderModel = require('../models/orderModel');
const userModel = require('../models/userModel');

//-----------------------lấy hết dữ liệu order
const getAllOrder = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    orderModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all order',
         drink: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu voucher bằng id
//-------------------------get a voucher by ID 
const getAnOrderById = (request,response) => {
    //b1: thu thập dữ liệu
    const orderId = request.params.orderId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${orderId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    orderModel.findById(orderId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one orderId ',
             voucher: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
}

 //------------------insert one order ---------------------------
 const insertAnOrder = (request, response) => {
    //b1: thu thập dữ liệu
    const body = request.body;
    const voucherId = request.params.voucherId;
    const drinkId = request.params.drinkId;
    const userId = request.params.userId;
    console.log(body);
    //b2: validate dữ liệu
   
    if(!body.orderCode){
        return response.status(400).json({
           status: 'Bad request',
           message: 'orderCode ko hop le'
        })
    }
    if(!body.pizzaSize){
        return response.status(400).json({
           status: 'Bad request',
           message: 'pizzaSize ko hop le'
        })
    }
    if(!body.pizzaType){
        return response.status(400).json({
           status: 'Bad request',
           message: 'pizzaType ko hop le'
        })
    }
    // if(isNaN(body.status) || body.phone < 0){
    //     return response.status(400).json({
    //        status: 'Bad request',
    //        message: 'phone ko hop le'
    //     })
    // }

    // b3: gọi model tạo dữ liệu 
    const newOrder = new orderModel( {
        //  _id: mongoose.Types.ObjectId,
        orderCode : body.orderCode,
        pizzaSize : body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: voucherId,
        drink: drinkId,
        status: "await"
    })
    console.log(newOrder);
   
    orderModel.create(newOrder)
    .then((data) => {
        //  response.status(201).json({
        //     message: 'Successful to create new user',
        //     user: data
        //  })
         userModel.findByIdAndUpdate(userId, {$push: { order: data._id }})
            .then((data) => {
                response.status(201).json({
                message: 'Successful to update new user',
                user: data
                })
            
             })
            .catch((error)=> {
                response.status(500).json({
                    message: `internal sever ${error}`
                })
            })
    })
    .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })

 }
//------------------update one order ---------------------------
const updateAnOrder = (request, response) => {
    //b1: thu thập dữ liệu
    const body = request.body;
    const voucherId = request.params.voucherId;
    const drinkId = request.params.drinkId;
    const orderId = request.params.orderId;
    console.log(body);
    //b2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return response.status(400).json({
           status: 'Bad Request',
           message: `${orderId} ko hợp lệ`
        })
     }
    if(!body.orderCode){
        return response.status(400).json({
           status: 'Bad request',
           message: 'orderCode ko hop le'
        })
    }
    if(!body.pizzaSize){
        return response.status(400).json({
           status: 'Bad request',
           message: 'pizzaSize ko hop le'
        })
    }
    if(!body.pizzaType){
        return response.status(400).json({
           status: 'Bad request',
           message: 'pizzaType ko hop le'
        })
    }
    // if(isNaN(body.status) || body.phone < 0){
    //     return response.status(400).json({
    //        status: 'Bad request',
    //        message: 'phone ko hop le'
    //     })
    // }

    // b3: gọi model tạo dữ liệu 
    const newOrder =  {
        //  _id: mongoose.Types.ObjectId,
        orderCode : body.orderCode,
        pizzaSize : body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: voucherId,
        drink: drinkId,
        status: body.status
    }
    console.log(newOrder);
   
    orderModel.findByIdAndUpdate(orderId,newOrder)
    .then((data) => {
         response.status(201).json({
            message: 'Successful to create new order',
            user: data
         })
    })
    .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })

 }
 //-----------------delete an order
 const deleteAnOrder = (request,response) => {
    //b1: thuu thập dữ liệu
    const orderId = request.params.orderId;
    //b2: validate an order
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return response.status(400).json({
           status: 'Bad Request',
           message: `${orderId} ko hợp lệ`
        })
    }
    //b3: delete dữ liệu
    orderModel.findByIdAndDelete(orderId)
    .then((data) => {
        response.status(201).json({
           message: 'Successful to delete an order',
           user: data
        })
   })
   .catch((error)=> {
     response.status(500).json({
        message: `internal sever ${error}`
     })
   })
 }
 module.exports = {
    insertAnOrder,
    updateAnOrder,
    getAllOrder,
    getAnOrderById,
    deleteAnOrder
 }