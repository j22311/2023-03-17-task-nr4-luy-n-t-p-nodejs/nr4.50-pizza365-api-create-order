const { request, response } = require('express');
const mongoose = require('mongoose');

const orderModel = require('../models/orderModel');

const userModel = require('../models/userModel');

const voucherModel = require('../models/voucherModel');

const drinkModel = require('../models/drinkModel');

const orderHandle = (request,response) => {
    const body = request.body;
    const email = body.email;
    const maNuocUong = body.idLoaiNuocUong;
    const maVoucher = body.idVoucher;
    var IdDrink ;
    var IdVoucher = "0";
    
    voucherModel.findOne({maVoucher : maVoucher})
    .catch((error)=> {
        response.status(500).json({
           message: `internal sever ${error}`
        })
    })
    //kiểm tra voucher có tồn tại và gán giá trị Id cho IDvoucher
    .then((dataVoucher) => {
        if(dataVoucher){
            IdVoucher = dataVoucher._id;
            console.log('_idVoucher: ' + IdVoucher);
        }
     }) 
    drinkModel.findOne({maNuocUong: maNuocUong})
    .catch((error)=> {
        response.status(500).json({
           message: `internal sever ${error}`
        })
    })
    //kiểm tra drink có tồn tại và gán giá trị cho IDDrink 
    .then((dataDrink) => {
        if(dataDrink){
            IdDrink = dataDrink._id;
            console.log('_id Drink:' + IdDrink);
        }
     }) 
    const condition = {};

    if(email){
        condition.email = {$eq: email}
    }
     //tìm kiếm user xem user này đã tồn tại chưa
    userModel.findOne(condition)
    .catch((errorUserFind)=> {
        return response.status(500).json({
           message: `internal sever ${errorUserFind}`
        })
      })
    .then((dataUserFind) => {
        //user này có tồn tại trong hệ thống
       if(dataUserFind){
        console.log(dataUserFind);
            const newOrder = new orderModel( {
                pizzaSize : request.body.kichCo,
                pizzaType: request.body.loaiPizza,
                drink: IdDrink,
                status: "confirmed"
            });
            console.log('------newOrder: ' + newOrder)
            //id voucher có tồn tại trong hệ thống
            if(IdVoucher !== "0"){
                newOrder.voucher = IdVoucher;
            }
            console.log('------newOrder: ' + newOrder)
            orderModel.create(newOrder)
            .catch((errorOrder)=> {
                return response.status(500).json({
                   message: `internal sever on create an Order ${errorOrder}`
                })
              })
            .then((dataOrder) => {
                console.log(dataOrder)
                const idOrder = dataOrder._id.toString();
                    //lấy ordercode từ ký tự 0 đến 10;
                    console.log(idOrder);
                    const orderCode = idOrder.slice(0,10);
                //cập nhật orderCode theo Id của Order
                orderModel.findByIdAndUpdate(dataOrder._id,{orderCode: orderCode})
                .catch((error)=> {
                    response.status(500).json({
                       message: `internal sever ${error}`
                    })
                })
                //update lại order vào dữ liệu user
                .then((dataOrderUpdate) => {
                    userModel.findByIdAndUpdate(dataUserFind._id, {$push: {order: dataOrderUpdate._id}})
                    .catch((error)=> {
                        response.status(500).json({
                            message: `internal sever ${error}`
                        })
                    })
                    .then((dataUserUpdate) => {
                        response.status(201).json({
                        message: 'Successfully to insert new order',
                        user: dataUserUpdate
                        })
                    
                    })
                 })
             })
       }else{
            const newUser = {
                fullName: body.hoTen,
                email: body.email,
                address: body.diaChi,
                phone: body.soDienThoai,
            }

            userModel.create(newUser)
            .catch((error)=> {
                response.status(500).json({
                   message: `internal sever ${error}`
                })
            })
            .then((dataUserCreate) => {
                console.log(dataUserFind);
            const newOrder = new orderModel( {
                pizzaSize : request.body.kichCo,
                pizzaType: request.body.loaiPizza,
                drink: IdDrink,
                status: "confirmed"
            });
            console.log('------newOrder: ' + newOrder)
            //id voucher có tồn tại trong hệ thống
            if(IdVoucher !== "0"){
                newOrder.voucher = IdVoucher;
            }
            console.log('------newOrder: ' + newOrder)
            orderModel.create(newOrder)
            .catch((errorOrder)=> {
                return response.status(500).json({
                   message: `internal sever on create an Order ${errorOrder}`
                })
              })
            .then((dataOrder) => {
                console.log(dataOrder)
                const idOrder = dataOrder._id.toString();
                    //lấy ordercode từ ký tự 0 đến 10;
                    console.log(idOrder);
                    const orderCode = idOrder.slice(0,10);
                //cập nhật orderCode theo Id của Order
                orderModel.findByIdAndUpdate(dataOrder._id,{orderCode: orderCode})
                .catch((error)=> {
                    response.status(500).json({
                       message: `internal sever ${error}`
                    })
                })
                //update lại order vào dữ liệu user
                .then((dataOrderUpdate) => {
                    userModel.findByIdAndUpdate(dataUserCreate._id, {$push: {order: dataOrderUpdate._id}})
                    .catch((error)=> {
                        response.status(500).json({
                            message: `internal sever ${error}`
                        })
                    })
                    .then((dataUserUpdate) => {
                        response.status(201).json({
                        message: 'Successfully to insert new order',
                        user: dataUserUpdate
                        })
                    
                    })
                 })
             })
             })
       }
    })
}

const getAllDrinksbyDev = (request,response) => {
    drinkModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all drinks',
         drink: data
      })
   })
      .catch((error)=> {
         response.status(500).json({
            message: `internal sever ${error}`
         })
      })
}
module.exports = {
    orderHandle,
    getAllDrinksbyDev
}