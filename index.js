//khai báo thư viện express 
const express = require("express");


const { drinkRouter } = require("./app/routes/drinkRouter");
const { orderRouter } = require("./app/routes/orderRouter");
const { userRouter } = require("./app/routes/userRouter");
const { voucherRouter } = require("./app/routes/voucherRouter");


//khai báo app
const app = express();

// khai báo port 
const port = 8000;
// Cấu hình request đọc được body json
app.use(express.json());

//hàm này chạy ghi ra ngày tháng trong terminal
app.use((req,res,next) => {
    console.log(new Date());

    next();
})
//b1khai báo thư viện mongoose
var mongoose = require('mongoose');
const { orderHandleRouter } = require("./app/routes/orderHandleRouter");
// b2: kết nối với mongoosebd
main().catch(err => console.log(err));

async function main() {
  await mongoose.connect('mongodb://127.0.0.1:27017/CRUD_Pizza365');
  console.log('Successfully connected mongoDB');
  
}

//khai báo drinksRounter để chạy trên postMan Task 506.30 Pizza365 Drink router 
app.use('/', drinkRouter)

//khai báo voucherRounter để chạy trên postMan Task 506.40 Pizza365 Drink router 
app.use('/',voucherRouter);

//khai báo userRouter để chạy trên postMan Task 506.50 Pizza365 Drink router 
app.use('/',userRouter);

//khai báo orderRouter để chạy trên postMan Task 506.60 Pizza365 Drink router 
app.use('/',orderRouter)

//khai báo orderRouter để chạy trên postMan Task 506.60 Pizza365 Drink router 
app.use('/',orderHandleRouter);

// -----khỡi động app qua cổng------
app.listen(port, () => {
    console.log(`app listen on port ${port}`);
}) 
